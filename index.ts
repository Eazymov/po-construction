import { errorIntervalsSample } from './data';

function calcErrorsCount(
  errorIntervals: number[],
  integralCharacteristic: number,
) {
  const intervalsCount = errorIntervals.length;
  let errorsCount = intervalsCount + 1;
  let diffFG = 1;

  do {
    errorsCount++;

    let Fn = 0;
    const Gn = intervalsCount / (errorsCount - integralCharacteristic);

    for (let index = 0; index < intervalsCount; index += 1) {
      Fn += 1 / (errorsCount - (index + 1));
    }

    diffFG = Fn - Gn;
  } while (diffFG > 0);

  errorsCount -= 1;

  return errorsCount;
}

function calcIntegralCharacteristic(errorIntervals: number[]) {
  const intervalsCount = errorIntervals.length;
  let sumA1 = 0;
  let sumA2 = 0;

  for (let index = 0; index < intervalsCount; index += 1) {
    sumA1 += (index + 1) * errorIntervals[index];
    sumA2 += errorIntervals[index];
  }

  return sumA1 / sumA2;
}

function calcScalingFactor(errorIntervals: number[], veracity: number) {
  let denomK = 0;

  for (let index = 0; index < errorIntervals.length; index += 1) {
    denomK += (veracity - (index + 1) + 1) * errorIntervals[index];
  }

  return errorIntervals.length / denomK;
}

function calcTestingTime(
  errorsCount: number,
  scalingFactor: number,
  intervalsCount: number,
) {
  let sum = 0;

  for (let index = 1; index < errorsCount - intervalsCount; index += 1) {
    sum += 1 / index;
  }

  return (1 / scalingFactor) * sum;
}

function calcData(errorIntervals: number[]) {
  const integralCharacteristic = calcIntegralCharacteristic(errorIntervals);

  const errorsCount = calcErrorsCount(errorIntervals, integralCharacteristic);

  const intervalsCount = errorIntervals.length;

  const veracity = errorsCount - 1;

  const scalingFactor = calcScalingFactor(errorIntervals, veracity);

  const nextErrorDetectionTime = 1 / (scalingFactor * (veracity - intervalsCount));

  const testingTime = calcTestingTime(
    errorsCount,
    scalingFactor,
    intervalsCount,
  );

  return {
    errorsCount,
    testingTime,
    nextErrorDetectionTime,
  };
}

function log() {
  const data = calcData(errorIntervalsSample);

  console.log(`Общее количество ошибок: ${data.errorsCount.toFixed()}`);
  console.log(
    `Время до окончания тестирования: ${data.testingTime.toFixed(2)}`,
  );
  console.log(
    `Время до появления следующей ошибки: ${data.nextErrorDetectionTime.toFixed(
      2,
    )}`,
  );
}

log();
